# Use latest openjdk image as the base
FROM openjdk:8-jdk-alpine

#labels
LABEL maintainer "emmanuel.lesne@middleware-solutions.fr"
LABEL version="1.0"
LABEL description="Image of WSO2 API Management Server."
LABEL fr.middlewaresolutions.wso2.em.version="2.1.0"
LABEL fr.middlewaresolutions.jdk.version="OpenJDK-8"

# Set the EI_VERSION env variable
ENV AM_VERSION 2.1.0
ENV AM_HOME /opt/wso2am-$AM_VERSION

# USER root
# Add package system
RUN apk --no-cache add wget
RUN apk --no-cache add zip
RUN apk --no-cache add bash

# cf https://github.com/wso2/product-am/releases
COPY product-apim-$AM_VERSION.zip /opt

RUN mkdir /opt && \
  cd /opt && \
#  wget https://github.com/wso2/product-apim/archive/v$AM_VERSION.zip && \
  unzip wso2am-$AM_VERSION.zip

# Expose the ports we're interested in
EXPOSE 8243 9443 8280

# Set the default command to run on boot
WORKDIR $AM_VERSION
CMD /opt/wso2am-$AM_VERSION/bin/wso2server.sh
